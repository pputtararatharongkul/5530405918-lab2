package puttarathamrongkul.panupong.lab2;
/*
* This program to learn how to understand Java data types
* Create by Panupong Puttarathamrongkul
* 553040591-8 Lab2
*/
class DataTypes {
	public static void main(String args[]) {
// to insert right argument
		if((args.length == 3) && (args[2].length() == 11)){
			System.out.println("My name is " + args[0] + " " + args[1]);
			System.out.println("My student ID was " + args[2]);
		}else {
			System.err.println("Enter your <First name> <Last name> <ID same as xxxxxxxxx-x>");
			System.exit(0);
		}
		System.out.print(args[0].substring(0,1) + " ");
//boolean variable = true
		boolean b = true;
		System.out.print(b);
//to given first and last two digits of ID
		String first = args[2].substring(0,1) + args[2].substring(1,2);
		String last = args[2].substring(8,9) + args[2].substring(10,11);
//to given long and float as (last two digit).(first two digit)
		String ldotf = last + "." + first;
//to convert digits of ID string's to integer and declare a variable with an octal value and a hexadecimal value
		int lastint = Integer.parseInt(last);
		int oct = 011;
		int hex = 0x00;
		oct = lastint;
		hex = lastint;
//to print a variable
		System.out.println(" " + oct + " " + hex);
		System.out.println(Long.parseLong(last) + " " + Float.parseFloat(ldotf) + " " + Double.parseDouble(ldotf));
	}
}