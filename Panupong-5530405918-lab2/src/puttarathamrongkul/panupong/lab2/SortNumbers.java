package puttarathamrongkul.panupong.lab2;
import java.util.Arrays;
/*
* This program to learn how to sorted number
* Create by Panupong Puttarathamrongkul
* 553040591-8 Lab2
*/
public class SortNumbers {
	public static void main(String[] args) {
		
		double a[] = new double[args.length];
		a[0] = Double.parseDouble(args[0]);
		System.out.println("For the input numbers:");
// store string number to an array
		for(int i=0;i<args.length;i++){
			a[i] = Double.parseDouble(args[i]);
			System.out.print(a[i] + " ");
		}
		System.out.println("\nSorted numbers:");
//sorting number
		Arrays.sort(a, 0, args.length);
//print sorted number
		for(int j=0;j<args.length;j++){
			System.out.print(a[j] + " ");
		}
	}
}