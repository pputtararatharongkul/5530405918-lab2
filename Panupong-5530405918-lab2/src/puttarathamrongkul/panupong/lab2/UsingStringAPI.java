package puttarathamrongkul.panupong.lab2;
/*
* This program to learn how to using String API
* Create by Panupong Puttarathamrongkul
* 553040591-8 Lab2
*/
public class UsingStringAPI {
	public static void main(String[] args) {
		//to ignore case Upper or Lower arguments 
		String schoolName = args[0].toLowerCase();
		int a = schoolName.indexOf("school");
		int b = schoolName.indexOf("college");
		if(a >= 0){
			System.out.println(args[0] + " has school at " + a);
		}else if(b >= 0){
			System.out.println(args[0] + " has college at " + b);
		}else{
			System.out.println(args[0] + " does not contain school or college");
		}
			
	}

}