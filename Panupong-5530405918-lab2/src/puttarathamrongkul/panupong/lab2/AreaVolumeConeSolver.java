package puttarathamrongkul.panupong.lab2;
public class AreaVolumeConeSolver {
/*
* This program to find area and volume of cone
* create by Panupong Puttarathamrongkul
* 553040591-8 Lab2
*/
	public static void main(String[] args) {
		if(args.length == 3){
			System.out.println("For cone with r " + args[0] + " s " + args[1] + " h " + args[2]);
			//convert string number to a double value
			double r = Double.parseDouble(args[0]);
			double s = Double.parseDouble(args[1]);
			double h = Double.parseDouble(args[2]);
			//math
			double area = Math.PI*r*s+Math.PI*2*r;
			double volume = Math.PI*r*2*h*1/3;
			System.out.print("Surface area is  " + area);
			System.out.print(" volume is " + volume);
		}else{
			System.err.print("AreaVolumeConeSolver <r> <s> <h>");
		}
	}
}
